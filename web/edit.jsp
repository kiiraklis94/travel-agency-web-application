<%-- 
    Document   : edit
    Created on : 02-May-2016, 22:10:57
    Author     : kiiraklis94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import ="java.sql.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                           url="jdbc:mysql://localhost:3306/logindb"
                           user="root"  password="root"/>
        <title>Edit</title>
    </head>
    <%
        String editID = request.getParameter("editID");
        String category1 = request.getParameter("category1");
        String category2 = request.getParameter("category2");
        String country = request.getParameter("country");
        String city = request.getParameter("city");
        String youtube = request.getParameter("youtube");

        pageContext.setAttribute("editID", editID);
        pageContext.setAttribute("category1", category1);
        pageContext.setAttribute("category2", category2);
        pageContext.setAttribute("country", country);
        pageContext.setAttribute("city", city);
        pageContext.setAttribute("youtube", youtube);
    %>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 panel panel-default">
                    <img src="logo.png" alt="Logo" style="width:325px; height:58px;">
                </div>            
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4 panel panel-default">
                    <h1 class="text-center login-title">Edit destination</h1>
                    <div class="form-group">
                        <form method="post" action="editController.jsp">

                            <label for="category1">Category 1:</label>
                            <select class="form-control" name="category1" >
                                <option selected>${category1}</option>
                                <option>Winter</option>
                                <option>Christmas</option>
                                <option>Summer</option>
                            </select>

                            <label for="category1">Category 2:</label>
                            <select class="form-control" name="category2" >
                                <option selected>${category2}</option>
                                <option>Winter</option>
                                <option>Christmas</option>
                                <option>Summer</option>
                            </select>

                            <label for="country">Country:</label>
                            <input type="text" name="country" value="${country}" class="form-control" required>

                            <label for="city">City:</label>
                            <input type="text" name="city" value="${city}" class="form-control" required>

                            <label for="youtube">Youtube URL:</label>
                            <input type="text" name="youtube" value="${youtube}" class="form-control" required>

                            <input type="hidden" name="editID" value="${editID}" />
                            <br>

                            <a href="main.jsp" class="text-center btn btn-lg btn-danger">Go back to Main</a>
                            <button class="btn btn-lg btn-primary text-center" type="submit">Save Changes</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>


