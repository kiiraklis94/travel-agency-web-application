<%@ page import ="java.sql.*" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    </head>

    <div class="container panel panel-default text-center">
        <%
            String userid = request.getParameter("uname");
            String pwd = request.getParameter("pass");

            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/logindb",
                    "root", "root");
            Statement stUser = con.createStatement();
            Statement stPwd = con.createStatement();

            ResultSet rsUser, rsPwd;

            rsUser = stUser.executeQuery("select * from users where username='" + userid + "';");
            rsPwd = stPwd.executeQuery("select * from users where password='" + pwd + "';");

            if (rsUser.next() && rsPwd.next()) {
                session.setAttribute("userid", userid);
                response.sendRedirect("../main.jsp");

            } else {
                out.println("<h2>User doesn't exist or the password is invalid.</h2>  <a href='../index.html' class='btn btn-primary'>Try again</a> or "
                        + "<a href='reg.jsp' class='btn btn-primary'>Register a new account</a>");
            }
        %>
    </div>
</html>