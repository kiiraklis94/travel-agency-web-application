<html>
     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    </head>
    
    <body>
        <div class="container panel panel-default text-center">
            <h1>Registration was Successful.</h1>
            <a href='../index.html' class="btn btn-primary">Go to Login Page</a>
        </div>
    </body>

</html>