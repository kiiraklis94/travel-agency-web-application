<%-- 
    Document   : addnew
    Created on : 29-Apr-2016, 13:46:17
    Author     : kiiraklis94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add new destination</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 panel panel-default">
                    <img src="logo.png" alt="Logo" style="width:325px; height:58px;">
                </div>            
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4 panel panel-default">
                    <h1 class="text-center login-title">Add new destination</h1>
                    <div class="form-group">
                        <form method="post" action="addnewController.jsp">

                            <label for="category1">Category 1:</label>
                            <select class="form-control" name="category1" required>
                                <option>Winter</option>
                                <option>Christmas</option>
                                <option>Summer</option>
                            </select>
                            
                            <label for="category1">Category 2:</label>
                            <select class="form-control" name="category2">
                                <option></option>
                                <option>Winter</option>
                                <option>Christmas</option>
                                <option>Summer</option>
                            </select>
                            
                            <label for="country">Country:</label>
                            <input type="text" name="country" class="form-control" required>
                            
                            <label for="city">City:</label>
                            <input type="text" name="city" class="form-control" required>
                            
                            <label for="youtube">Youtube URL:</label>
                            <input type="text" name="youtube" class="form-control" required>
                            <br>
                            <a href="main.jsp" class="text-center btn btn-lg btn-danger">Go back to Main</a>
                            <button class="btn btn-lg btn-primary" type="submit">Add</button>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
