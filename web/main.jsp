<%-- 
    Document   : main
    Created on : 27-Apr-2016, 18:45:34
    Author     : kiiraklis94
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                           url="jdbc:mysql://localhost:3306/logindb"
                           user="root"  password="root"/>
        <title>Main Page</title>
    </head>

    <body class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 panel panel-default">
                <img src="logo.png" alt="Logo" style="width:325px; height:58px;">
            </div>            
        </div>

        <div class="row">
            <div class="col-md-10 col-md-offset-1 panel panel-default">
                <div class="panel panel-heading">
                    <div class='panel panel-default text-right'>
                        <c:set var="user" scope="session" value="${userid}" />
                        <c:if test="${ (user == null) || (user == '') }">
                            <% response.sendRedirect("index.html");%>
                        </c:if>
                        <c:if test="${ (user != null) || (user != '') }">
                            Welcome, <b><%=session.getAttribute("userid")%></b>.
                            <a href='login/logout.jsp' class="btn btn-xs btn-primary">Log out</a>
                        </c:if>
                    </div>
                    <h2>Επιλέξτε κατηγορίες προορισμών:</h2>
                </div>
                <div class="col-md-12 panel panel-default ">
                    <form class="form-inline" role="form" method="get" action="main.jsp">
                        <label class="radio-inline">
                            <input type="radio" name="category" value="All" checked="checked">Όλοι
                        </label>

                        <sql:query dataSource="${snapshot}" var="countCat" >
                            SELECT * FROM sum WHERE category='Winter';
                        </sql:query>
                        <c:forEach var="row" items="${countCat.rows}">
                            <label class="radio-inline">
                                <input type="radio" name="category" value="Winter">Χειμερινοί (${row.sum})
                            </label>
                        </c:forEach>

                        <sql:query dataSource="${snapshot}" var="countCat">
                            SELECT sum FROM sum WHERE category='Christmas';
                        </sql:query>
                        <c:forEach var="row" items="${countCat.rows}">
                            <label class="radio-inline">
                                <input type="radio" name="category" value="Christmas">Χριστουγεννιάτικοι (${row.sum})
                            </label>
                        </c:forEach>

                        <sql:query dataSource="${snapshot}" var="countCat">
                            SELECT sum FROM sum WHERE category='Summer';
                        </sql:query>   
                        <c:forEach var="row" items="${countCat.rows}">
                            <label class="radio-inline">
                                <input type="radio" name="category" value="Summer">Καλοκαιρινοί (${row.sum})
                            </label>
                        </c:forEach>
                        -----
                        <button type="submit" class="btn btn-primary btn-sm" value="Refresh results">Refresh Results</button>

                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 panel panel-default">  
                <sql:query dataSource="${snapshot}" var="count">
                    SELECT sum FROM sum WHERE category='All';
                </sql:query>
                <c:forEach var="row" items="${count.rows}">
                    <c:set var="countAll" value="${row.sum + countAll}" />
                </c:forEach>
                There are <b>${countAll}</b> total entries - 
                <a href="addnew.jsp" class="btn btn-primary btn-sm">Add New</a>
            </div>            
        </div>      

        <div class="row">
            <div class="col-md-10 col-md-offset-1 panel panel-default">
                <%
                    String category = request.getParameter("category");
                    pageContext.setAttribute("category", category);
                %>

                <c:if test="${category != 'All'}">
                    <sql:query dataSource="${snapshot}" var="result" >
                        SELECT * FROM destinations WHERE category1 = ? OR category2=?;
                        <sql:param value="${category}" />
                        <sql:param value="${category}" />
                    </sql:query>
                </c:if>
                <c:if test="${category == 'All' || category == null}">
                    <sql:query dataSource="${snapshot}" var="result" >
                        SELECT * FROM destinations;
                    </sql:query>
                </c:if>

                <h5>Showing ${result.rowCount} result(s):</h5>

                <table class="table table-hover table-bordered">
                    <tr>
                        <th>Details</th>
                        <th>Categories</th>
                        <th>Country</th>
                        <th>City</th>
                        <th>YouTube Video</th>
                        <th>Created/<br>Last Edited By</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    <c:forEach var="row" items="${result.rows}">
                        <tr>
                            <td>
                                <form method="get" action="viewDest.jsp">
                                    <button type="submit" class="btn btn-default glyphicon glyphicon-eye-open" name="dID" value="${row.D_id}" />
                                    <input type="hidden" name ="category1" value="${row.category1}" />
                                    <input type="hidden" name ="category2" value="${row.category2}" />
                                    <input type="hidden" name ="country" value="${row.country}" />
                                    <input type="hidden" name ="city" value="${row.city}" />
                                    <input type="hidden" name ="youtube" value="${row.youtube}" />
                                    <input type="hidden" name ="createdby" value="${row.createdby}" />
                                </form>
                            </td>
                            <td>${row.category1}<br>${row.category2}</td>
                            <td>${row.country}</td>
                            <td><b>${row.city}</b></td>
                            <td><a href="${row.youtube}" target="_blank">${row.youtube}</a></td>
                            <td>${row.createdby}</td>
                            <td>
                                <form method="post" action="edit.jsp">
                                    <button type="submit" class="btn btn-default glyphicon glyphicon-edit" name="editID" value="${row.D_id}" />
                                    <input type="hidden" name ="category1" value="${row.category1}" />
                                    <input type="hidden" name ="category2" value="${row.category2}" />
                                    <input type="hidden" name ="country" value="${row.country}" />
                                    <input type="hidden" name ="city" value="${row.city}" />
                                    <input type="hidden" name ="youtube" value="${row.youtube}" />
                                </form>
                            </td>
                            <td>
                                <form method="get" action="delete.jsp">
                                    <button type="submit" class="btn btn-danger glyphicon glyphicon-trash" name="deleteID" value="${row.D_id}" />
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </body>
</html>
