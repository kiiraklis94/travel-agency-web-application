<%-- 
    Document   : editController
    Created on : 04-May-2016, 08:54:54
    Author     : kiiraklis94
--%>

<%@ page import ="java.sql.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                           url="jdbc:mysql://localhost:3306/logindb"
                           user="root"  password="root"/>
        <title>Editing...</title>
    </head>
    <%
        String editID = request.getParameter("editID");
        String category1 = request.getParameter("category1");
        String category2 = request.getParameter("category2");
        String country = request.getParameter("country");
        String city = request.getParameter("city");
        String youtube = request.getParameter("youtube");

        pageContext.setAttribute("editID", editID);
        pageContext.setAttribute("category1", category1);
        pageContext.setAttribute("category2", category2);
        pageContext.setAttribute("country", country);
        pageContext.setAttribute("city", city);
        pageContext.setAttribute("youtube", youtube);
    %>
    <sql:update dataSource="${snapshot}" var="results">
        UPDATE destinations SET createdby=?, category1=?, category2=?, country=?, city=?, youtube=?
        WHERE D_id=?;
        <sql:param value="${userid}" />
        <sql:param value="${category1}" />
        <sql:param value="${category2}" />
        <sql:param value="${country}" />
        <sql:param value="${city}" />
        <sql:param value="${youtube}" />
        <sql:param value="${editID}" />
    </sql:update>
    <% response.sendRedirect("main.jsp");%>