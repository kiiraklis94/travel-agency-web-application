<%-- 
    Document   : addnewController
    Created on : 03-May-2016, 17:16:47
    Author     : kiiraklis94
--%>

<%@ page import ="java.sql.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                           url="jdbc:mysql://localhost:3306/logindb"
                           user="root"  password="root"/>
        <title>Deleting...</title>
    </head>
</html>
<% 
    String deleteID = request.getParameter("deleteID");
    pageContext.setAttribute("deleteID", deleteID);
%>

<sql:update dataSource="${snapshot}" var="results">
    DELETE FROM destinations WHERE D_id = ?;
    <sql:param value="${deleteID}" />
</sql:update> 

<% response.sendRedirect("main.jsp");        %>