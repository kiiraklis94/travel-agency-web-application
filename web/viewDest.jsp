<%-- 
    Document   : viewDest
    Created on : 02-May-2016, 22:26:47
    Author     : kiiraklis94
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%--<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>--%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
                           url="jdbc:mysql://localhost:3306/logindb"
                           user="root"  password="root"/>
        <title>Details Page</title>
    </head>
    <%
        String dID = request.getParameter("dID");
        String category1 = request.getParameter("category1");
        String category2 = request.getParameter("category2");
        String country = request.getParameter("country");
        String city = request.getParameter("city");
        String youtube = request.getParameter("youtube");
        String createdby = request.getParameter("createdby");

        String[] ytID = youtube.split("v=");

        pageContext.setAttribute("dID", dID);
        pageContext.setAttribute("category1", category1);
        pageContext.setAttribute("category2", category2);
        pageContext.setAttribute("country", country);
        pageContext.setAttribute("city", city);
        pageContext.setAttribute("youtube", youtube);
        pageContext.setAttribute("ytID", ytID[1]);
        pageContext.setAttribute("createdby", createdby);
    %>

    <body class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <img src="logo.png" alt="Logo" style="width:325px; height:58px;">
                </div>
            </div>            
        </div>

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default text-center">
                    <div class="panel-body">
                        <div class="col-md-4">
                            <a href="main.jsp" class="btn btn-primary">Go Back to Main</a>
                        </div>
                        <div class="col-md-4">
                            <form method="post" action="edit.jsp" >
                                <button type="submit" class="btn btn-default glyphicon glyphicon-edit" name="editID" value="${dID}" /> Edit
                                <input type="hidden" name ="category1" value="${category1}" />
                                <input type="hidden" name ="category2" value="${category2}" />
                                <input type="hidden" name ="country" value="${country}" />
                                <input type="hidden" name ="city" value="${city}" />
                                <input type="hidden" name ="youtube" value="${youtube}" />
                            </form>
                        </div>
                        <div class="col-md-4">
                            <form method="get" action="delete.jsp">
                                <button type="submit" class="btn btn-danger glyphicon glyphicon-trash" name="deleteID" value="${dID}" /> Delete
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel panel-heading"><h3>Wikipedia Article</h3></div>
                    <div class="panel panel-body">
                        <iframe src="https://en.m.wikipedia.org/wiki/${city}" width="920" height="360" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        <h1>Details</h1>
                    </div>
                    <div class="panel panel-body">
                        <h3><b>Category 1:</b> ${category1}</h3>
                        <h3><b>Category 2:</b> ${category2}</h3>
                        <h3><b>Country:</b> ${country}</h3>
                        <h3><b>City:</b> ${city}</h3>
                        <h4><b>Created/Last Edited by:</b> ${createdby}</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel panel-heading">
                        <h1>Video</h1>
                    </div>
                    <div class="panel panel-body">
                        <iframe width="520" height="360" src="https://www.youtube.com/embed/${ytID}" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>



    </body>

</html>
